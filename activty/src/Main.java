import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = scanner.nextLine();

        System.out.println("Last Name:");
        String lastName = scanner.nextLine();

        System.out.println("First Subject Grade:");
        double num1 = scanner.nextDouble();

        System.out.println("Second Subject Grade:");
        double num2 = scanner.nextDouble();

        System.out.println("Third Subject Grade:");
        double num3 = scanner.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName);

        double number = (num1 + num2 + num3) / 3;
        int rounded = (int) number;

        System.out.println("Your grade average is: " + rounded);

    }
}